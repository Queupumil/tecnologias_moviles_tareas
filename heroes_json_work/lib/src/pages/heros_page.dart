import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:heroes_json_work/src/components/hero_card.dart';
import 'package:heroes_json_work/src/model/hero_club_model.dart';
import 'package:heroes_json_work/src/model/hero_model.dart';
import 'package:heroes_json_work/src/services/heros_consumer.dart';

class HerosPage extends StatefulWidget {
  HerosPage({Key key}) : super(key: key);

  @override
  _HerosPageState createState() => _HerosPageState();
}

class _HerosPageState extends State<HerosPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: FutureBuilder(
            future: HerosConsummer().getLista(),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                print("hay datos: ${snapshot.hasData}");
                List<HeroCard> heroList = [];
                HerosClubModel clubModel = snapshot.data;
                for (HeroModel hero in clubModel.heros) {
                  heroList.add(HeroCard(
                    hero: hero,
                  ));
                }

                return Center(
                  child: Column(
                    children: [
                      Container(
                        margin: EdgeInsets.only(top: 30.0),
                        child: Text(
                          "Equipo: ${utf8.decode(clubModel.nombre.runes.toList())}",
                          style: TextStyle(fontSize: 20.0,
                          fontWeight: FontWeight.w800),
                        ),
                      ),
                      Column(
                        children: heroList,
                      )
                    ],
                  ),
                );
              }
              return Container(
                child: Text("Connection Lost"),
              );
            }),
      ),
    );
  }
}
