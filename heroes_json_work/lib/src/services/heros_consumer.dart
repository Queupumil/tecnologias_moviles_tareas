import 'dart:convert';

import 'package:heroes_json_work/src/model/hero_club_model.dart';
import 'package:http/http.dart' as http;

class HerosConsummer {
  final String _serverUrl = 'https://www.hectoralvarez.dev/icc727/';

  Map<String, String> _headers = {
    'Content-Type': 'application/json',
    'Authorization': 'mi\$up34Token'
  };

  Future<HerosClubModel> getLista() async {
    final response =await http.get(this._serverUrl + 'heroes.json', headers: this._headers);
    print('GET: ${response.statusCode}');
    
    if (response.statusCode == 200) {
      //print(response.body);
      return HerosClubModel.fromJson(json.decode(response.body));
    }

    return HerosClubModel(nombre: "Sin club", heros: []);
  }

}
