import 'package:json_annotation/json_annotation.dart';

part 'hero_model.g.dart';

@JsonSerializable()
class HeroModel {
  final String nombre;
  final String poder;
  final String icon;
  final String color;

  HeroModel({
    this.nombre,
    this.poder,
    this.icon,
    this.color

  });

   factory HeroModel.fromJson(Map<String, dynamic> json) =>
    _$HeroModelFromJson(json);

  Map<String, dynamic> toJson() => _$HeroeModelToJson(this);


}