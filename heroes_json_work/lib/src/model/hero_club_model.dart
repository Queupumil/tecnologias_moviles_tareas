import 'package:json_annotation/json_annotation.dart';

import 'package:heroes_json_work/src/model/hero_model.dart';

part 'hero_club_model.g.dart';

@JsonSerializable()
class HerosClubModel{
  final String nombre;
  final List<HeroModel> heros;

  HerosClubModel({this.nombre, this.heros});

  factory HerosClubModel.fromJson(Map<String, dynamic> json) => 
  _$HerosClubModelFromJson(json);

  Map<String, dynamic> toJson() => _$MisHeroesModelToJson(this);
}