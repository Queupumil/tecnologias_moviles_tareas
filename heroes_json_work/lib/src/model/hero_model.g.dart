// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hero_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HeroModel _$HeroModelFromJson(Map<String, dynamic> json) {
  return HeroModel(
    nombre: json['nombre'] as String,
    poder: json['poder'] as String,
    icon: json['icon'] as String,
    color: json['color'] as String,
  );
}

Map<String, dynamic> _$HeroeModelToJson(HeroModel instance) =>
    <String, dynamic>{
      'nombre': instance.nombre,
      'poder': instance.poder,
      'icon': instance.icon,
      'color': instance.color,
    };
