// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'hero_club_model.dart';
// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HerosClubModel _$HerosClubModelFromJson(Map<String, dynamic> json) {
  return HerosClubModel(
    nombre: json['nombre'] as String,
    heros: (json['heroes'] as List)
        ?.map((e) =>
            e == null ? null : HeroModel.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$MisHeroesModelToJson(HerosClubModel instance) =>
    <String, dynamic>{
      'nombre': instance.nombre,
      'heroes': instance.heros,
    };
