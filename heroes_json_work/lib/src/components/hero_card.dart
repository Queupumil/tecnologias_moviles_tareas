import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:heroes_json_work/src/model/hero_model.dart';
import 'package:heroes_json_work/src/utils/hero_icon_maps.dart';

class HeroCard extends StatelessWidget {
  final HeroModel hero;

  const HeroCard({Key key, this.hero}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 400.0,
      child: Card(
        child: Column(
          children: [
            //TITULO
            Text(
              utf8.decode(hero.nombre.runes.toList()),
              style: TextStyle(color: getColor(),
              fontWeight: FontWeight.w600,
              fontStyle: FontStyle.italic,
              fontSize: 20.0 ),
            ),
            //
            Row(
              children: [
                //imagen
                Container(
                  margin: EdgeInsets.only(left: 15.0),
                  child: Icon(HeroIconMaps().getIcon(hero.icon),
                  color: getColor(),
                  size: 50.0,
                  ),
                ),
                SizedBox(
                  width: 60.0,
                ),
                //poder
                Text("Poder: ${utf8.decode(hero.poder.runes.toList()) }",
                style: TextStyle(fontSize: 17.0),)
              ],
            )
          ],
        ),
      ),
    );
  }

  getColor() {
    String color ="ff"+ hero.color.replaceAll("#", "");
    return Color(int.parse(color,radix:16));
  }
}
