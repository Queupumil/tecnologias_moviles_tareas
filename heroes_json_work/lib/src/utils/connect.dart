import 'dart:async';

import 'package:connectivity/connectivity.dart';

class Connected {
  Future<bool> isConnected() async {
    var result = await (Connectivity().checkConnectivity());

    return result == ConnectivityResult.mobile ||
        result == ConnectivityResult.wifi;
  }
}
