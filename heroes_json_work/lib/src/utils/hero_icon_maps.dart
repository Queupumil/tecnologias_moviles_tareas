
import 'package:flutter/material.dart';

class HeroIconMaps{

  Map<String, IconData> _iconsMaps = {
    "accessibility_new":Icons.accessibility_new,
    "ac_unit":Icons.ac_unit,
    "android":Icons.android,
    "healing":Icons.healing,
    "access_time":Icons.access_time,
    "monetization_on":Icons.monetization_on,
    "offline_bolt":Icons.offline_bolt,
    "title":Icons.title,
    "library_music":Icons.library_music,
    
    
  };

  IconData getIcon(String icon){
    return _iconsMaps[icon];
  }
  

}