import 'package:flutter/material.dart';
import 'package:login_work/src/routes/routes.dart';
import 'package:login_work/src/utils/data.dart';
import 'package:login_work/src/view/home_view.dart';
import 'package:login_work/src/view/login_view.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: FutureBuilder(
          future: Data().getData("logedIn"),
          builder: (context, snapshot) {
            bool logedIn = false;
            print(snapshot.data);
            if (snapshot.hasData) {
              logedIn = snapshot.data == "true";
            }
            if (logedIn) {
              return HomeView();
            }
            return LoginView();
          }),
      routes: getRoutes(),
    );
  }
}
