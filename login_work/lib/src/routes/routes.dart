import 'package:flutter/cupertino.dart';
import 'package:login_work/src/view/home_view.dart';
import 'package:login_work/src/view/login_view.dart';
import 'package:login_work/src/view/register_view.dart';

Map <String,WidgetBuilder> getRoutes(){
  return <String,WidgetBuilder>{
    LoginView.tag:(context) => LoginView(),
    RegisterView.tag:(context) => RegisterView(),
    HomeView.tag:(context)=> HomeView()
  };
}