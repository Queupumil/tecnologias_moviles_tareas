import 'package:flutter/material.dart';
import 'package:login_work/src/utils/data.dart';
import 'package:login_work/src/utils/validator.dart';
import 'package:login_work/src/view/home_view.dart';
import 'package:login_work/src/view/register_view.dart';

class LoginView extends StatefulWidget {
  LoginView({Key key}) : super(key: key);
  static String tag = '/login';
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  String _email = "";
  String _password = "";
  String _errorMessage = "";
  Color _errorColor = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("lib/assets/fondo.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: Container(
              padding: EdgeInsets.only(top: 200),
              height: 500.0,
              margin: EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                children: [
                  Text(
                    "Login",
                    style: TextStyle(fontSize: 30, color: Colors.white),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextField(
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Correo",
                      labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    onChanged: (value) => _email = value,
                  ),
                  TextField(
                    obscureText: true,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Contraseña",
                      labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    onChanged: (value) => _password = value,
                  ),
                  Text(
                    _errorMessage,
                    style: TextStyle(color: _errorColor),
                  ),
                  MaterialButton(
                    onPressed: () => login(),
                    child: Text(
                      "Ingresar",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  MaterialButton(
                    onPressed: () => signin(),
                    child: Text("Registrarse",
                        style: TextStyle(color: Colors.white)),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> login() async {
    print("email:$_email password:$_password");
    if (_email == "" || _password == "") {
      trowMessageError("Falta llenar alguno de los campos");
      return;
    } else if (!Validator().emailFormatValidator(_email)) {
      trowMessageError("El formato del correo es incorrecto");
      return;
    }

    //el correo esta bien escrito y no falta llenar ningun campo 
    bool exist = await Data().checkData(_email);
    if (exist) {
      //El correo existe.
      String _passExist = await Data().getData(_email);
      if (_passExist == _password) {
        Data().saveData("logedIn", true.toString());
        Data().saveData("userLogedIn", _email);
        Navigator.pushNamed(context, HomeView.tag);
        return;
      } else {
        // La contraseña esta incorrecta.
        trowMessageError("Correo o contraseña invalido");
        return;
      }
    } else {
      //El correo no existe.
      trowMessageError("No se encuentra registrado en la aplicacion.");
    }
  }

  void signin() {
    Navigator.pushNamed(context, RegisterView.tag);
  }

  void trowMessageError(String message) {
    setState(() {
      _errorMessage = message;
      _errorColor = Colors.white;
    });
  }
}
