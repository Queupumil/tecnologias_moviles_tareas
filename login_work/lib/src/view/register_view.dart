import 'package:flutter/material.dart';
import 'package:login_work/src/utils/data.dart';
import 'package:login_work/src/utils/validator.dart';

class RegisterView extends StatefulWidget {
  RegisterView({Key key}) : super(key: key);
  static String tag = '/register';

  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  String _email = "";
  String _password = "";
  String _rePassword = "";
  String _errorMessage = "";
  Color _errorColor = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            decoration: new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("lib/assets/fondo.jpg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          Center(
            child: Container(
              padding: EdgeInsets.only(top: 150),
              height: 500.0,
              margin: EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                children: [
                  Text(
                    "Registrarse",
                    style: TextStyle(fontSize: 30, color: Colors.white),
                  ),
                  SizedBox(
                    height: 15,
                  ),
                  TextField(
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Correo",
                      labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    onChanged: (value) => _email = value,
                  ),
                  TextField(
                    obscureText: true,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Contraseña",
                      labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    onChanged: (value) => _password = value,
                  ),
                  TextField(
                    obscureText: true,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: "Repetir la contraseña",
                      labelStyle: TextStyle(color: Colors.white),
                      enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(
                          color: Colors.white,
                        ),
                      ),
                    ),
                    onChanged: (value) => _rePassword = value,
                  ),
                  Text(
                    _errorMessage,
                    style: TextStyle(color: _errorColor),
                  ),
                  MaterialButton(
                    onPressed: () => register(),
                    child: Text(
                      "Registrarse",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                  MaterialButton(
                    onPressed: () => Navigator.pop(context),
                    child: Text(
                      "Volver",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> register() async {
    if (_email == "" || _password == "" || _rePassword == "") {
      trowMessageError("Falta llenar alguno de los campos");
      return;
    }else if(!Validator().emailFormatValidator(_email)){
      trowMessageError("El formato del correo es incorrecto");
      return;
    }

    bool _exist = await Data().checkData(_email);
    if (_exist) {
      trowMessageError("El correo actual se encuentra registrado");
      return;
    } else if (_rePassword != _password) {
      trowMessageError("Las contraseñas no coinciden");
      return;
    } else {
      Data().saveData(_email, _password);
      Navigator.pop(context);
    }
  }

  void trowMessageError(String message) {
    setState(() {
      _errorMessage = message;
      _errorColor = Colors.red;
    });
  }
}
