import 'package:flutter/material.dart';
import 'package:login_work/src/utils/data.dart';
import 'package:login_work/src/view/login_view.dart';

class HomeView extends StatefulWidget {
  HomeView({Key key}) : super(key: key);
  static String tag = '/home';
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: EdgeInsets.only(top: 200),
        height: 500.0,
        margin: EdgeInsets.symmetric(horizontal: 20.0),
        child: Center(
          child: Column(
            children: [
              FutureBuilder(
                future: Data().getData("userLogedIn"),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Text("Usuario: ${snapshot.data}");
                  } else {
                    return Text("Usuario: null");
                  }
                },
              ),
              SizedBox(
                height: 10.0,
              ),
              MaterialButton(
                onPressed: () => logout(),
                child: Text("Salir"),
                color: Colors.blue,
              )
            ],
          ),
        ),
      ),
    );
  }

  logout() {
    Data().saveData("logedIn", false.toString());
    Navigator.pushReplacementNamed(context, LoginView.tag);
  }
}
